# Cucumber & Pupeteer E2E tests demo

[![pipeline status](https://gitlab.com/DanielRamosAcosta/the-moviedb-e2e-tests/badges/master/pipeline.svg)](https://gitlab.com/DanielRamosAcosta/the-moviedb-e2e-tests/commits/master)

[![coverage report](https://gitlab.com/DanielRamosAcosta/the-moviedb-e2e-tests/badges/master/coverage.svg)](https://gitlab.com/DanielRamosAcosta/the-moviedb-e2e-tests/commits/master)

This repo is a demo for creating E2E tests using Puppeteer and Cucumber.
