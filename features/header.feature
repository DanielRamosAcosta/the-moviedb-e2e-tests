Feature: The header
  The header should be able to make the user reach the most common places

  Scenario: Reach the Add New Movie page
    Given the user has logged in
    And I am at the home page
    When I hover the "plus icon" at the header
    And I click the "Add New Movie" link at the plus popup
    Then I see the "Create a New Movie" page

  Scenario: Reach the Login page
    Given I am at the home page
    When I click in "Login" at the header
    Then I see the "Login" page

  Scenario: Reach the Sign Up page
    Given I am at the home page
    When I click in "Sign Up" at the header
    Then I see the "Sign Up" page
