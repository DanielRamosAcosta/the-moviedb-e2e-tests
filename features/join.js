export const joinActions = (...fns) => async (ctx, ...args) => {
  for (let i = 0; i < fns.length; i++) {
    await fns[i](ctx, args[i]);
  }
}

export const joinAssertions = joinActions
