Feature: Login page
  Users should be able to login into the platform

  Scenario: User logs in and sees its profile
    Given I start tracing
    Given I am at the home page
    When I click in "Login" at the header
    And I enter my username "testuser123999" and my password "foobar123"
    And I press the "submit" button at the form
    Then I see the "Profile" page
    And I save a screenshot at "profile-new-user.png"
    And I stop tracing
