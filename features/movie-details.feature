Feature: Movie details page
  Users wants to see the details

  Scenario Outline: See the top billed cast of Pulp Fiction
    Given I am at the movie details page of "Pulp Fiction"
    Then I see "<actor>" as "<character>" at billed cast
    Examples:
        |       actor       |     character     |
        |     John Travolta |      Vincent Vega |
        | Samuel L. Jackson |    Jules Winfield |
        |       Uma Thurman |       Mia Wallace |
        |      Bruce Willis |    Butch Coolidge |
        |       Ving Rhames | Marsellus Wallace |
  
  Scenario Outline: See the genres of Pulp Fiction
      Given I am at the movie details page of "Pulp Fiction"
      Then I see "Thriller" at right sidebar in the Genre list
      And I see "Crime" at right sidebar in the Genre list
      And I see "Action" at right sidebar in the Genre list

