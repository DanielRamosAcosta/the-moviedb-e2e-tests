import path from 'path'
import mkdirp from 'mkdirp-promise'
import { selectors } from './selectors'
import { navigation } from './navigation'
import { joinActions } from '../join';

//#region utilities

function getSelector(selectorScope, elementKey) {
  let currentSelector = selectorScope[elementKey]
  if (!currentSelector) {
    throw new Error(`No selector for ${elementKey}. Avaiables: ${Object.keys(selectorScope)}`)
  }
  const selector = typeof currentSelector === 'string' ? currentSelector : currentSelector._root
  return selector
}

function getUrl(navigationScope, pageKey) {
  const url = navigationScope[pageKey]
  if (!url) {
    throw new Error(`There is no url for "${pageKey}", available: ${JSON.stringify(navigationScope)}`)
  }

  return url
}

const clickElement = selectorScope => async (ctx, elementKey) => {
  const selector = getSelector(selectorScope, elementKey)

  const page = await ctx.getPage()
  await page.waitForSelector(selector)
  await page.click(selector)
}

const hoverElement = selectorScope => async (ctx, elementKey) => {
  const selector = getSelector(selectorScope, elementKey)

  const page = await ctx.getPage()
  await page.waitForSelector(selector)
  await page.hover(selector)
}

const typeAt = selector => async (ctx, value) => {
  const page = await ctx.getPage()
  await page.waitForSelector(selector)
  await page.type(selector, value)
}

//#endregion

export const pause = () => new Promise(() => {})

export const sleep = (ctx, seconds) => new Promise((r) => setTimeout(r, seconds * 1e3))

export async function navigateTo(ctx, pageKey) {
  const page = await ctx.getPage()
  await page.goto(getUrl(navigation, pageKey))
  await page.reload()
}

const typeUsernameAtLogin = typeAt(selectors.login.usernameInput)
const typePasswordAtLogin = typeAt(selectors.login.passwordInput)

export const enterUsernameAndPassword = joinActions(
  typeUsernameAtLogin,
  typePasswordAtLogin
)

export const pressSubmit = clickElement(selectors.commonElements)

export async function takeScreenshot(ctx, screenshotPath) {
  const SCREENSHOT_PATH = path.resolve(__dirname, '../../screenshots')
  
  await mkdirp(SCREENSHOT_PATH)
  const page = await ctx.getPage()
  await page.screenshot({path: path.join(SCREENSHOT_PATH, screenshotPath)})
}

export async function withLoggedUser (ctx) {
  await navigateTo(ctx, 'login'),
  await enterUsernameAndPassword(ctx, 'testuser123999', 'foobar123')
  await pressSubmit(ctx, 'submit')
  await sleep(ctx, 1)
}

export const clickElementAtHeaderRightMenu = clickElement(selectors.header.rightMenu)

export const hoverElementAtHeaderRightMenu = hoverElement(selectors.header.rightMenu)

export const clickElementAtHeaderPlusIconPopup = clickElement(selectors.header.rightMenu['plus icon'])

export async function startTracing (ctx) {
  const page = await ctx.getPage()
  await page.tracing.start({ path: 'trace.json' });
}

export async function stopTracing (ctx) {
  const page = await ctx.getPage()
  await page.tracing.stop();
}
