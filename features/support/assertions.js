import assert from 'assert'
import { selectors } from "./selectors";
import { joinAssertions } from '../join';

const getTextOf = (page, selector) =>
  page.evaluate(selector => document.querySelector(selector).textContent, selector)

const getTextListOf = (page, selector) => 
  page.evaluate(selector => [...document.querySelectorAll(selector)].map(e => e.textContent), selector)

const textExistsInElements = selector => async (ctx, element) => {
  const page = await ctx.getPage()
  const elements = await getTextListOf(page, selector);
  assert(elements.includes(element), `Expected "${element}" to be in ${JSON.stringify(elements)}`);
}

const elementHasText = selector => async (ctx, expectedText) => {
  const page = await ctx.getPage()
  await page.waitForSelector(selector);
  const currentText = await getTextOf(page, selector);
  assert.ok(new RegExp(expectedText).test(currentText), `Element to have "${expectedText}" but is "${currentText}"`);
}

export const currentPageIs = elementHasText(selectors.title)

export const rightSidebarGenreListHasGenre = textExistsInElements(selectors.movieDetails.eachGenre)

export const actorAsCharacterAtBilledCast = joinAssertions(
  textExistsInElements(selectors.movieDetails.topBilled.eachActor.actorName),
  textExistsInElements(selectors.movieDetails.topBilled.eachActor.characterName)
)
