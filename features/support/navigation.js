export const navigation = {
  home: 'https://www.themoviedb.org',
  'Pulp Fiction': 'https://www.themoviedb.org/movie/680-pulp-fiction',
  login: 'https://www.themoviedb.org/login'
};