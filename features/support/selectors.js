export function nestedSelector(rootSelector, nestedSelectors) {
  return Object.entries(nestedSelectors).reduce((previousValue, [key, value]) => {
    let currentSelector = typeof value === "string"
      ? `${rootSelector} ${value}`
      : nestedSelector(rootSelector, value)

    return {
      ...previousValue,
      ...(previousValue._root ? {} : {_root: rootSelector}),
      [key]: currentSelector
    }
  }, {})
}

export const selectors = {
  title: 'title',
  header: nestedSelector('header', {
    rightMenu: nestedSelector('.right .primary', {
      'plus icon': nestedSelector('li:first-child > div', {
        'Add New Movie': 'a[href="/movie/new"]'
      }),
      Login: 'a[href="/login"]',
      'Sign Up': 'a[href="/account/signup"]',
    })
  }),
  login: {
    usernameInput: '#username',
    passwordInput: '#password'
  },
  movieDetails: {
    topBilled: nestedSelector('section.top_billed', {
      eachActor: nestedSelector('.card', {
        actorName: 'p a',
        characterName: '.character',
      })
    }),
    eachGenre: 'section.genres li'
  },
  commonElements: {
    submit: `main input[type="submit"]`
  }
};
