import { URL } from 'url'
import { AfterAll, After, BeforeAll, setDefaultTimeout } from 'cucumber'
import scope from './scope'
import puppeteer from 'puppeteer'

setDefaultTimeout(10 * 1000);

const isCi = Boolean(process.env.CI)

const CI_OPTIONS = {
  args: [
    '--no-sandbox',
    '--disable-setuid-sandbox',
    '--disable-dev-shm-usage'
  ],
  executablePath: '/usr/bin/google-chrome'
}

BeforeAll(async () => {
  scope.driver = puppeteer
  scope.browser = await scope.driver.launch({
    headless: isCi,
    defaultViewport: {
      width: 1920,
      height: 1080
    },
    ...(isCi ? CI_OPTIONS : {})
  })
  console.log(`Devtools Protocol listening at ${(new URL(scope.browser.wsEndpoint())).port}`)
})

After(async function() {
  await this.closePage()
})

AfterAll(async () => {
  if (scope.browser) await scope.browser.close();
})
