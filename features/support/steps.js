import { Given, When, Then } from '../utils/keywords';
import {
  enterUsernameAndPassword,
  navigateTo,
  pressSubmit,
  takeScreenshot,
  withLoggedUser,
  pause,
  clickElementAtHeaderRightMenu,
  hoverElementAtHeaderRightMenu,
  clickElementAtHeaderPlusIconPopup,
  startTracing,
  stopTracing,
} from './actions'
import { rightSidebarGenreListHasGenre, currentPageIs, actorAsCharacterAtBilledCast } from './assertions';

Given('I am at the {word} page', navigateTo)
When('I click in {string} at the header', clickElementAtHeaderRightMenu)
When('I enter my username {string} and my password {string}', enterUsernameAndPassword)
When('I press the {string} button at the form', pressSubmit)
Then('I see the {string} page', currentPageIs);
Then('I save a screenshot at {string}', takeScreenshot)
Given('the user has logged in', withLoggedUser)
Then('I pause', pause)
When('I hover the {string} at the header', hoverElementAtHeaderRightMenu)
When('I click the {string} link at the plus popup', clickElementAtHeaderPlusIconPopup)
Given('I am at the movie details page of {string}', navigateTo)
Then('I see {string} as {string} at billed cast', actorAsCharacterAtBilledCast)
Then('Then I see {string} at right sidebar in the Genre list', rightSidebarGenreListHasGenre)
Given('I start tracing', startTracing)
Then('I stop tracing', stopTracing)
