import { setWorldConstructor } from 'cucumber'
import pti from 'puppeteer-to-istanbul'
import scope from './scope'

class CustomWorld {
  constructor() {
    this._browser = scope.browser
    this._page = null
    
  }

  async getPage () {
    if (!this._page) {
      this._page = await this._browser.newPage();
      await this._page.setExtraHTTPHeaders({'Accept-Language': 'en'});
      /* await Promise.all([
        this._page.coverage.startJSCoverage(),
        this._page.coverage.startCSSCoverage()
      ]); */
    }
    return this._page
  }

  async closePage () {
    if (this._page) {
      //Retrive the coverage objects
      /* const [jsCoverage, cssCoverage] = await Promise.all([
        this._page.coverage.stopJSCoverage(),
        this._page.coverage.stopCSSCoverage(),
      ]);
      pti.write(jsCoverage) */
      // Wait for https://github.com/GoogleChrome/puppeteer/issues/645
      const client = await this._page.target().createCDPSession();
      await client.send('Network.clearBrowserCookies');
      await this._page.close()
      this._page = null
    }
  }
}

setWorldConstructor(CustomWorld)
