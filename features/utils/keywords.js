import * as cucumber from 'cucumber'

const stepRegex = /\{(([a-zA-Z0-9-_]+):)?([a-zA-Z-_]+)\}/g

function wrapThe(GivenWhenThen) {
  return function (stepString, handler) {
    // MIrar cantidad de argumentos del stepString
    
    const matcehs = stepString.match(stepRegex)
    const paramNumber = matcehs ? matcehs.length : 0
  
    return GivenWhenThen(stepString, new Proxy(function(...args) {return handler(this, ...args)}, {
      get (target, name){
        if (name === "length") {
          return paramNumber
        }
        return target[name]
      }
    }))
  }
}

export const Given = wrapThe(cucumber.Given)
export const Then = wrapThe(cucumber.Then)
export const When = wrapThe(cucumber.When)
